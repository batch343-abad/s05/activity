
USE classic_models

SELECT customerName FROM customers WHERE country = "Philippines";

SELECT contactLastName, contactFirstName FROM customers WHERE customerName = "La Rochelle Gifts";

SELECT productName, MSRP FROM products WHERE productName = "The Titanic";

SELECT firsTName, lastName FROM employees WHERE email = "jfirrelli@classicmodelcars.com";

SELECT customerName FROM customers WHERE state IS NULL;

SELECT firsTName, lastName FROM employees WHERE lastName = "Patterson" AND firsTName = "Steve";

SELECT customerName, country, creditLimit FROM customers WHERE country != "USA" AND creditLimit > 3000;

SELECT customerNumber FROM orders WHERE comments LIKE "%D%H%L%";

SELECT productLine FROM productlines WHERE textDescription LIKE "%State of the art%";

SELECT DISTINCT country FROM customers;

SELECT DISTINCT status FROM orders;

SELECT customerName, country FROM customers WHERE country = "USA" OR country = "FRANCE" OR country = "Canada";

SELECT employees.firsTName, employees.lastName, offices.city FROM employees JOIN offices
ON employees.officeCode = offices.officeCode
WHERE city = "Tokyo";

SELECT customers.customerName FROM customer JOIN employees
ON customers.salesRepEmployeeNumber = employees.employeeNumber
WHERE lastName = "Thompson" AND firsTName = "Leslie";

SELECT products.productName, customers.customerName
FROM orders
JOIN orderdetails ON orders.orderNumber = orderdetails.orderNumber
JOIN products ON orderdetails.productCode = products.productCode
JOIN customers ON orders.customerNumber = customers.customerNumber
WHERE customerName = "Baane Mini Imports";

SELECT employees.firsTName, employees.lastName, customers.customerName, offices.country FROM employees JOIN offices
ON employees.officeCode = offices.officeCode
JOIN customers
ON employees.employeeNumber = customers.salesRepEmployeeNumber
WHERE customers.country = offices.country;

SELECT products.productName, products.quantityInStock FROM products JOIN productlines 
ON products.productLine = productlines.productLine
WHERE productlines.productLine = "Planes" AND products.quantityInStock < 1000;

SELECT customerName FROM customerS WHERE phone LIKE "%+81%";
